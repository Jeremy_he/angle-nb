#include "bc95.h"

/**
  ******************************************************************************
  * @file    bc95.c
  * @author  控远智能科技有限公司
  * @version V1.0.0
  * @date    2017-8-1
  * @brief   BC95初始化及测试功能实现函数
  ******************************************************************************
  * @attention
  *
  *驱动主要基于BC95_check_ack，BC95_send_cmd这两个函数，移植这两个函数，自己可以根据需要实现更多功能
  *本驱动是参考正点原子sim900A相关代码修改而得，主要参考修改并实现了BC95_check_ack，BC95_send_cmd这两个函数
  *
  ******************************************************************************
  */



uint8_t check_ack_timeout = 10; //注网失败次数.超过这么多次数无应答后跳出循环
uint8_t ue_need_reboot_flag = 0;
uint8_t udp_restart_flag = 1;
uint8_t IMEI_number[16];

extern uint8_t rx_len;
extern uint8_t recv_end_flag;
extern uint8_t rx_buffer[128];


void delay_ms(int ms)
{
	int i,j;
   for(i=0;i<ms;i++)
	   for(j=0;j<1000;j++)
	   { ; }
}


//检查返回的响应是否符合预期
//传入参数为预期返回的字符串
//返回0，为检测到预期值
//其他值，预期字符所在的位置
uint8_t* BC95_check_ack(char *str)
{
	char *strx=0;
	if(recv_end_flag)		
	{ 
		rx_buffer[rx_len]='\0';
		strx=strstr((const char*)rx_buffer,(const char*)str);
	} 
	return (uint8_t*)strx;
}

//发生at指令函数
//cmd:at指令，ack：预期响应，waittime,超时时间
//返回0，发送成功
//返回1，发送超时
uint8_t BC95_send_cmd(char *cmd,char *ack,uint16_t waittime)
{
	uint8_t res=0; 
	recv_end_flag=0;
	rx_len = 0;
	uint8_t string_end[2]={0x0D,0x0A};
	uint8_t num_to_send;//
	num_to_send = strlen(cmd);
	memset(rx_buffer,0,BUFFER_SIZE);
//	printf("%s\r\n",cmd);
	HAL_UART_Transmit(&hlpuart1,cmd,num_to_send,5000);
	HAL_UART_Transmit(&hlpuart1,string_end,2,5000);//发送字符串结束符.
	
	if(ack&&waittime)
	{
		while(--waittime)	
		{
			delay_ms(50);
			if(recv_end_flag)
			{			
				if(BC95_check_ack(ack))break;
				recv_end_flag=0;
			} 
		}
		if(waittime==0)res=1; 
	}
	return res;
} 

//第一次对模块做的设置，然后以后每次唤醒后不用再设置.减少模块工作时间.
//包括IP和端口设置,BAND,自动连接模式.
void BC95_power_on(void)
{
//	if(udp_restart_flag)
//	{
//	   BC95_send_cmd(SET_UE_REBOOT,"REBOOT",300);
//  }	
	
	check_ack_timeout = 5;
	while(BC95_send_cmd("AT","OK",200)&&check_ack_timeout)
	{
		if(check_ack_timeout)
		{
			check_ack_timeout--;
//			printf("等待模块上电 %d.\r\n",check_ack_timeout);
		}
		delay_ms(200);
	}
	check_ack_timeout = 5;
	BC95_send_cmd(SET_UE_REBOOT,"REBOOT",300);
//	printf("reboot by AT\r\n");
	while(check_ack_timeout&&!BC95_check_ack("Neul"))
	{
		if(BC95_check_ack("Neul"))
		{
			break;
		}else
		{
			check_ack_timeout--;
			delay_ms(1000);
		}
	}	
	
	
	
	//判断模块是否是自动连接模式，如果不是则将模块设置成自动模式
//	if(BC95_send_cmd(QUERY_UE_CONNECT_MODE,SET_AUTO_CONNECT_TRUE,1000))
//	{ //可能是这条命令的回复内容太长，导致时钟无法正确判断"AUTOCONNECT,TRUE"是否收到，导致每次开机重启.
//		check_ack_timeout = 3;
//		while(check_ack_timeout)
//		{
//			check_ack_timeout--;
//			if(BC95_send_cmd(SET_UE_AUTOCONNECT,"OK",300))
//			{
//				printf("设置为自动模式成功！\r\n");
//				break;
//			}
//			delay_ms(2000);
//		}
//		ue_need_reboot_flag =1;
//	}
	//判断模块是否是默认设置频段，如果不是则设置成默认频段
	if(BC95_send_cmd(QUERY_UE_BAND,UE_DEFAULT_BAND,300))
	{
		BC95_send_cmd(SET_UE_DEFAULT_BAND,UE_DEFAULT_BAND,300);
//		printf("BAND:5\n");
		ue_need_reboot_flag = 1;
	}
	else
//		printf("It's already default band.\n");
	
	//设置目标服务器ip地址和端口
//	if(BC95_send_cmd(QUERY_CDP_SERVER_IP_PORT,UE_DEFAULT_CDP_SERVER_IP_PORT,300))
//	{
//	   BC95_send_cmd(SET_CDP_SERVER_IP_PORT,UE_DEFAULT_CDP_SERVER_IP_PORT,300);
//		 printf("设置服务器IP和端口.\r\n");
//	}
//	else
//		printf("IP&PORT: 122.112.245.34,5683.\n");
	//重启模块生效配置
	if(ue_need_reboot_flag)
	{
		ue_need_reboot_flag = 0;
		check_ack_timeout = 5;
		BC95_send_cmd(SET_UE_REBOOT,"REBOOT",300);
//		printf("重启模块！\r\n");
		while(check_ack_timeout&&!BC95_check_ack("Neul"))
		{
			if(BC95_check_ack("Neul"))
			{
				break;
			}else
			{
				check_ack_timeout--;
				delay_ms(2000);
			}
		}
	}
	if(check_ack_timeout==0)
	{
//	   printf("AT connect failed.\n");
	}
}



//检查模块的网络状态，检测器件LED1会闪烁，LED1常亮为附网注网成功
//此函数不检查联网状态，仅检查附网注网状态，联网状态可以使用BC95_send_cmd，单独检测
//附网注网失败或者超时返回0，返回1附网注网成功，返回2附网成功
uint8_t query_net_status(void)
{
	uint8_t res = 0;
	uint8_t attached_flag = 0;
	uint8_t registered_flag = 0;
	check_ack_timeout = 10;//尝试连接次数为10，如果这么多次后仍没有得到正确响应，则退出.


	
	while(!(attached_flag&&registered_flag)&&check_ack_timeout)
	{
		if(!BC95_send_cmd(QUERY_UE_SCCON_STATS,SET_UE_SCCON,300))
		{
			attached_flag = 1;
			registered_flag = 1;
			res = 1;
//			printf("附网、注网成功！r\n");
			break;
		}else
		{
			if(!attached_flag)
			{
				if(!BC95_send_cmd(QUERY_UE_ATTACH_STATS,UE_ATTACHED_STATS,300))
				{
//					printf("附网成功!\r\n");
					attached_flag = 1;
					res =2;
				}else
				{
//					printf("正在附网...%d\r\n",check_ack_timeout);
					attached_flag = 0;
				}
			}
			if(attached_flag&&!registered_flag)
			{
//				if(attached_flag&&!BC95_send_cmd(QUERY_UE_EREG_STATS,UE_EREGISTERED_STATS,300))
        if(attached_flag&&!BC95_send_cmd(QUERY_UE_EREG_STATS,UE_EREGISTERED_OK,300))
				{
//					printf("注网成功！\r\n");
					registered_flag = 1;
					res =1;
				}else
				{
//					printf("正在注网...\r\n");
					registered_flag = 0;
				}
			}				
		}
		check_ack_timeout--;
		delay_ms(200);
		BC95_send_cmd(QUERY_UE_SIGNAL_QTY,"OK",300);//获取信号强度.
		if(!check_ack_timeout&&!attached_flag&&!registered_flag)
		{
//			printf("附网、注网失败！\r\n");
		}
	}
	return res;
}


void byte2hex(const uint8_t c, char * p_out)
{
  unsigned char highbyte,lowbyte;
	highbyte = c >> 4;
	lowbyte = c & 0x0f;
	highbyte += 0x30;
	lowbyte += 0x30;
	if(highbyte > 0x39)
		highbyte = highbyte+0x07;
	else
		highbyte = highbyte;
	if(lowbyte > 0x39)
		lowbyte = lowbyte + 0x07;
	else lowbyte = lowbyte ; 
	p_out[0] = highbyte;
	p_out[1] = lowbyte;
	
 
}

//void send_COAP_message(uint8_t data)
//{
//   char message[50] = "AT+NMGS=3,3132";
//	 char message_temp[2]={0};
//	 printf("data=0X%x.\n",data);
//	 byte2hex(data,message_temp);
//	 strcat(message,message_temp);
//	 message[16] = '\0';//字符串结束符.
//	 printf("message:\n%s\n",message);
//	 if(BC95_send_cmd(SET_MESSAGE_INDICATION,"OK",300))
//	 {
//	    printf("set NSMI failed.\n");
//	 }
//	 else 
//		 printf("set NSMI success.\n");
//	 if(BC95_send_cmd(message,"+NSMI:SENT",1000))
//	 {
//      printf("coap message sent failed.\n");
//	 }
//	 else 
//		 printf("coap message sent successfully.\n");
//	 
//}



void send_UDP_data(uint8_t data)
{
	 uint8_t i;
	 uint8_t data_temp;
	 char message_imei_hex[8];
	 int message_length;
   char message[120] = SEND_UDP_DATA;
	 char message_temp[2]={0};
	 Query_IMEI_number();	 
	 for(i=0;i<4;i++)
		  byte2hex(IMEI_number[i+11],(message_imei_hex+2*i)); //取后4个字节
	 for(i=0;i<8;i++)
	   message[TOPIC_START_BYTE+i]=message_imei_hex[i];
//	 printf("data=0X%x.\n",data);
	 message_length = strlen(SEND_UDP_DATA);	 
	 
	 data_temp = data / 100+ '0';
	 byte2hex(data_temp,message_temp);	 
	 strcat(message,message_temp);	 
	 
	 data_temp = data % 100 /10 + '0';
	 byte2hex(data_temp,message_temp);	 
	 strcat(message,message_temp);	 
	 
	 data_temp = data % 10 +'0';
	 byte2hex(data_temp,message_temp);	 
	 strcat(message,message_temp);	 
	 
	 
	 message[message_length+6] = '\0';//字符串结束符.
	 
//	 printf("message=%s\n",message);
	 
			 if(BC95_send_cmd(CREATE_UDP_SOCKET,"OK",300))
			 {
//					printf("create udp socket failed.\n");
			 }
			 else 
//				 printf(" create udp socket success.\n"); 
			 check_ack_timeout = 5;
	     while(BC95_send_cmd(message,"+NSONMI",200)&&check_ack_timeout)
	     {
		     if(check_ack_timeout)
		     {
			     check_ack_timeout--;
//					 printf("send udp message failed.%d\n",check_ack_timeout);
					 
		     }	
         delay_ms(700);				 
	     }			 
       if(check_ack_timeout!=0)
//				  printf("udp message sent successfully.\n");	 
			      printf("sent.\n");
			 			 
			 if(BC95_send_cmd(CLOSE_UDP_SOCKET,"OK",300))
	     {
//	        printf("close UDP socket failed.\n");
	     }
//	     else 
//		      printf("close UDP socket success.\n");	 
//       udp_restart_flag= 1;//除了开机第一次发送不需要重启，其余都要重启后再发第二次数据.	 
	 
	 
}

#define IMEI_BEGIN_NUMBER 8



void Query_IMEI_number()
{
	 uint8_t i;
   if(!BC95_send_cmd(QUERY_IMEI_NUMBER,QUERY_IMEI_NUMBER_ANSWER,300))
	 {
		 for(i=0;i<15;i++)
	     IMEI_number[i] = rx_buffer[IMEI_BEGIN_NUMBER+i];
	 }
	 IMEI_number[15] = '\0';
//	 printf("IMEI number = %s.\n",IMEI_number);
}

void set_IMEI_to_topic()
{
   
}

//读取数据，截取接收缓存中所需的数据保存到des,pos为起始地址，len为截取长度
//void get_str_data(char* des,char pos,char len)
//{
//	memcpy(des,usart2_rcvd_buf+pos,len);
//}	

////创建UDP链接，传入本地UDP端口号，返回0-6的socket id号，
//uint8_t creat_UDP_socket(char* local_port)
//{
//	char data[10]="";
//	uint8_t socket_id = 7;
//	char temp[64]="AT+NSOCR=DGRAM,17,";
//	strcat(temp,local_port);
//	strcat(temp,",1");
//	if(!BC95_send_cmd(temp,"OK",100))
//	{
//		get_str_data(data,2,1);
//		socket_id = (uint8_t)myatoi(data);
//		USARTx_printf(USART3,"Socket创建成功，句柄ID--> %d！\r\n",socket_id);
//		return socket_id;
//	}
//	USARTx_printf(USART3,"Socket创建失败，已经创建或端口被占用！\r\n");
//	return socket_id;
//}
////发送数据函数，传入socket,主机IP，远程主机端口，数据长度，数据
////这里暂时使用字符串参数
////返回值0，发送成功（鉴于UDP为报文传输，数据主机是否接收到模块是无法确认的）
////返回值1，发送失败
//uint8_t send_UDP_msg(char *socket,char *hostIP,char *port,char *dataLen,char *data)
//{
//	char ptr[600]="AT+NSOST=";
//	strcat(ptr,socket);
//	strcat(ptr,",");
//	strcat(ptr,hostIP);
//	strcat(ptr,",");
//	strcat(ptr,port);
//	strcat(ptr,",");
//	strcat(ptr,dataLen);
//	strcat(ptr,",");
//	strcat(ptr,data);
//	if(!BC95_send_cmd(ptr,"OK",200))
//	{
//		USARTx_printf(USART3,"发送数据--> %s！\r\n",ptr);	
//		return 0;
//	}
//	return 1;
//}
////接收数据处理函数，暂不提供实现方法，
////我们实现的思路是将字符串通过“，”进行分割，再截取想要的数据。
//char *receive_udp(char *socket,char *dataLen)
//{
//	char ptr[20]="AT+NSORF=";
//	char ack[5] =",";
//	char i;
//	char *result;
//	strcat(ack,dataLen);
//	strcat(ack,",");
//	strcat(ptr,socket);
//	strcat(ptr,",");
//	strcat(ptr,dataLen);
//	if(BC95_send_cmd(ptr,ack,200))
//	{	
//		for(i=5;i>0;i--)
//		{
//			result = strtok(usart2_rcvd_buf,",");
//		}
//		return result;
//	}
//	return 0;
//}

//注网附网成功之后循环发送数据。
//void BC95_Test_Demo(void)
//{
//#ifdef BC95_PWR_ON_TEST
//	BC95_power_on();
//#endif
//	if(query_net_status())
//	{
//		creat_UDP_socket(UE_LOCAL_UDP_PORT);
//		while(1)
//		{
//			send_UDP_msg("0",SERVER_HOST_UDP_IP,SERVER_HOST_UDP_PORT,"3","112233");
//			led_send_blink = 6;
//			delay_ms(10000);
//			delay_ms(10000);
//			delay_ms(10000);
//		}
//	}
//}


/******************* (C) COPYRIGHT  ikongyuan.com 170801 *****END OF FILE****/


